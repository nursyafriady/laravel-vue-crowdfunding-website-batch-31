<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => '7dbeb071-76f0-4a28-a036-3cd391392d91',
            'role_id' => 1,
            'name'      => 'sanbercode',
            'username'      => 'sanbercode',
            'email'     => 'sanbercode@gmail.com',
            'email_verified_at' => now(),
            'password'  => bcrypt('12345678')
        ]);

        User::create([
            'id' => '2c149580-6e2f-11ec-90d6-0242ac120003',
            'role_id' => 'f5dad600',
            'name'      => 'user1',
            'username'      => 'user1',
            'email'     => 'user1@gmail.com',
            'email_verified_at' => now(),
            'password'  => bcrypt('12345678')
        ]);

        User::create([
            'id' => 'ab8f762c-6e39-11ec-90d6-0242ac120003',
            'role_id' => 'f5dad600',
            'name'      => 'user2',
            'username'      => 'user2',
            'email'     => 'user2@gmail.com',
            'password'  => bcrypt('12345678')
        ]);
    }
}
