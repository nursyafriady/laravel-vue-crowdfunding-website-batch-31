<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/route-1', 'HomeController@route1')->name('route1')->middleware(['auth', 'mail']);
Route::get('/route-2', 'HomeController@index')->name('home')->middleware(['auth', 'admin']);
