<?php

use Illuminate\Http\Request;

// Route::get('/tes', 'TestController');
Route::namespace('Auth\API')->group(function () {
    Route::post('/register', 'RegisterController');
    Route::post('/verification', 'VerificationController');
    Route::post('/regenerate-otp', 'RegenerateOtpController');
    Route::post('/update-password', 'PasswordController@update');
    Route::post('/login', 'LoginController');

    Route::namespace('User')->group(function () {
        Route::get('/profile/get-profile', 'UserController@getProfile');
        Route::post('/profile/update-profile', 'UserController@updateProfile');
    });
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
