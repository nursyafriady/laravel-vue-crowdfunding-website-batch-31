<?php

namespace App\Events;

use App\Otp_Code;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReGenerateOtpEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $newOtp;
    public $user;

    public function __construct(Otp_Code $newOtp)
    {
        $this->newOtp = $newOtp;
        $this->user = $newOtp->user;
    }

    // public function broadcastOn()
    // {
    //     return new PrivateChannel('channel-name');
    // }
}
