<?php

namespace App\Listeners;

use App\Mail\SendGenerateOtp;
use App\Events\ReGenerateOtpEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegenerateOtpEmailNotification implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    public function handle(ReGenerateOtpEvent $event)
    {
        Mail::to($event->user)->send(new SendGenerateOtp($event->user));
    }
}
