<?php

namespace App\Listeners;

use App\Events\ReGenerateOtpEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ReGenerateOtpCode implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    public function handle(ReGenerateOtpEvent $event)
    {
        return $event->newOtp;
    }
}
