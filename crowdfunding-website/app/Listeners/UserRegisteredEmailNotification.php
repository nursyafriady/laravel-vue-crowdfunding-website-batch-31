<?php

namespace App\Listeners;

use App\Mail\UserRegisteredEmail;
use App\Events\UserRegisteredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserRegisteredEmailNotification implements ShouldQueue
{
    public function __construct()
    {
        //
    }

    public function handle(UserRegisteredEvent $event)
    { 
        Mail::to($event->user)->send(new UserRegisteredEmail($event->user, $event->user->otp));
    }
}
