<?php

namespace App\Http\Controllers\Auth\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class PasswordController extends Controller
{
    public function update(Request $request)
    {
        request()->validate([
            'email' => ['email', 'required'],
            'password' => ['required', 'confirmed', 'min:8']
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            if ($user->email_verified_at !== null) {              
                $user->update(['password' => bcrypt($request->password)]);  

                return response()->json([
                    'response_code' => "00",
                    'response_message' => 'Password Berhasil Dirubah',
                    'data' => [
                        'user' => new UserResource($user),
                    ]
                ], 200);
            } else {
                return response()->json([
                    'response_code' => "01",
                    'response_message' => 'Belum Verifikasi Email',
                ], 200);
            }            
        } else {
            return response()->json([
                'response_code' => "01",
                'response_message' => 'Email Tidak Terdaftar',
            ], 200);
        }
    }
}
