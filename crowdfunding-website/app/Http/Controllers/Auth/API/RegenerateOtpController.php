<?php

namespace App\Http\Controllers\Auth\API;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\ReGenerateOtpEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class RegenerateOtpController extends Controller
{
    public function __invoke(Request $request)
    {
       request()->validate(['email' => ['email', 'required']]);
       $user = User::with('otp_code')->where('email', $request->email)->first();

       if ($user) {
            $user->otp_code->update([
                'otp' => mt_rand(000000, 999999),
                'valid_until' => Carbon::now()->addMinutes(2),
            ]);

            $newOtp = $user->otp_code;
            event(new ReGenerateOtpEvent($newOtp));
            
            return response()->json([
                'response_code' => "00",
                'response_message' => 'berhasil generate OTP, silahkan cek email',
                'data' => [
                    'user' => new UserResource($user),
                ]
            ], 200);        
       } else {
            return response()->json([
                'response_code' => "01",
                'response_message' => 'Email tidak ditemukan',
            ], 200);
       }
    }
}
