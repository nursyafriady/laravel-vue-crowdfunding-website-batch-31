<?php

namespace App\Http\Controllers\Auth\API;

use App\User;
use App\Otp_Code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        request()->validate([
            'name' => ['string', 'required'],
            'username' => ['string', 'required',  'unique:users,username'],
            'email' => ['email', 'required',  'unique:users,email'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
        ]);
       
        $random = mt_rand(000000, 999999);
        $otpCode = Otp_Code::create([
            'user_id' => $user->id,
            'otp' => $random,
            'valid_until' => Carbon::now()->addMinutes(2),
        ]);

        event(new UserRegisteredEvent($otpCode, $user));        

        return response()->json([
            'response_code' => "00",
            'response_message' => 'silahkan cek email',
            'data' => [
                'user' => new UserResource($user),
            ]
        ], 200);
    }
}
