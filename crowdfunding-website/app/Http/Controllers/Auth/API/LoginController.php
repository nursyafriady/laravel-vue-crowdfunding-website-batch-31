<?php

namespace App\Http\Controllers\Auth\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => ['email', 'required'],
            'password' => ['required']
        ]);

        $user = User::where('email', $request->email)->first();
        
        if ($user) {
            if ($user->email_verified_at !== null) {
                // return $user;                
                if (!$token = auth()->attempt($request->only('email', 'password'))) {
                    throw ValidationException::withMessages([
                        'email' => ['The provided credentials are incorrect.'],
                    ]);
                }        
                return response()->json([
                    'response_code' => "00",
                    'response_message' => 'User Berhasil Login',
                    'data' => [
                        'token' => $token,
                        'user' => new UserResource($user),
                    ]
                ], 200);
            } else {
                return response()->json([
                    'response_code' => "01",
                    'response_message' => 'Belum Verifikasi Email',
                ], 200);
            }
        } else {
            return response()->json([
                'response_code' => "01",
                'response_message' => 'Email Tidak Terdaftar',
            ], 200);
        }
    }
}
