<?php

namespace App\Http\Controllers\Auth\API;

use App\User;
use App\Otp_Code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class VerificationController extends Controller
{
    public function __invoke(Request $request)
    {
        request()->validate([
            'otp' => ['string', 'required'],
        ]);

        $verifiedValid = Carbon::now();
        
        $input_otp_code = request('otp');
        $otp_code = Otp_Code::with('user')->where('otp', $input_otp_code )->first();

        if ($otp_code) {
            if ($verifiedValid > $otp_code->valid_until) {
                return response()->json([
                    'response_code' => "01",
                    'response_message' => 'OTP Tidak Valid, Generate Ulang',
                ], 200);
            } else {
                // return $otp_code;
                $email_verified = $otp_code->user->email_verified_at = $verifiedValid; 
                $user = User::find($otp_code->user_id);
                $user->email_verified_at = $email_verified;
                $user->save();

                $otp_code->delete();
                
                return response()->json([
                    'response_code' => "00",
                    'response_message' => 'Sukses Verifikasi OTP, Silahkan Login',
                    'data' => [
                        'user' => new UserResource($user),
                    ]
                ], 200);
            }
        } else {
            return response()->json([
                'response_code' => "01",
                'response_message' => 'OTP Code Tidak Ditemukan',
            ], 200);
        }
    }
}
