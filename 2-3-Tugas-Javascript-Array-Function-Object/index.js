// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort().forEach(function (item) {
    console.log(item);
});
console.log("\n");

// Soal 2
function introduce(data) {
    return (
      "Nama saya " +
      data.name +
      ", umur saya " +
      data.age +
      " tahun," +
      " alamat saya di " +
      data.address +
      ", dan saya punya hobby yaitu " +
      data.hobby
    );
  }
  
var data = {
    name : "John", 
    age : 30, 
    address : "Jalan Pelesiran", 
    hobby : "Gaming" 
} 

var perkenalan = introduce(data)
console.log(perkenalan) 
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
console.log("\n");

// Soal 3
function hitung_huruf_vokal(string) {
    var lowerCase = string.toLowerCase();
    var hasil = "";
    for (var i = 0; i < lowerCase.length; i++) {
      if (
        lowerCase[i] == "a" ||
        lowerCase[i] == "e" ||
        lowerCase[i] == "i" ||
        lowerCase[i] == "u" ||
        lowerCase[i] == "o"
      ) {
        hasil += string[i];
      }
    }
    return hasil.length;
}
  
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
var hitung_3 = hitung_huruf_vokal("Linaoe")
console.log(hitung_1 , hitung_2, hitung_3) // 3 2 4
console.log("\n");

// Soal 4
function hitung(angka) {
    return (2 * angka) - 2;
}

console.log( hitung(-2) ) // -6
console.log( hitung(-1) ) // -4
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(4) ) // 6
console.log( hitung(5) ) // 8
console.log( hitung(6) ) // 10