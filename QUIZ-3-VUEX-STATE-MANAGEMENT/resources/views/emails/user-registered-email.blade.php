<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OTP Notification</title>
</head>
<body>
    <p>
        <strong class="text-uppercase font-weight-bold">{{ $name }}</strong> 
        Kode OTP Anda adalah 
        <strong class="text-uppercase font-weight-bold">{{ $otpCode }}</strong> 
    </p>
</body>
</html>