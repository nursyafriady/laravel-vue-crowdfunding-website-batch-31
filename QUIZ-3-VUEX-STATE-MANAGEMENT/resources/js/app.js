import axios from 'axios'
window.axios = axios
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

Vue.component('navigation', require('./components/Navigation.vue').default);
Vue.component('footer-component', require('./components/Footer.vue').default);
Vue.component('loader-component', require('./components/Loader.vue').default);
Vue.component('campaign-item', require('./components/CampaignItem.vue').default);

import Vue from 'vue'
import router from './router/router.js'
import App from './App.vue'
import AxiosPlugin from 'vue-axios-cors';
import { BootstrapVue, BootstrapVueIcons  } from 'bootstrap-vue'
import store from './store/index'

Vue.use(AxiosPlugin)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        App
    }
});


// require('./bootstrap');
// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


// Ini saya hidden
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

