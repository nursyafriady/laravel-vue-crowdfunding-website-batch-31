import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    linkActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'home',
            alias: '/home',
            component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
        },
        {
            path: '/donations',
            name: 'donations',
            component: () => import(/* webpackChunkName: "donations" */ '../views/Donations.vue')
        },
        {
            path: '/campaigns',
            name: 'campaigns',
            component: () => import(/* webpackChunkName: "campaigns" */ '../views/Campaigns.vue')
        },
        {
            path: '/campaign/:id',
            name: 'campaign',
            component: () => import(/* webpackChunkName: "campaign" */ '../views/Detail.vue')
        },
        {
            path: '/blogs',
            name: 'blogs',
            component: () => import(/* webpackChunkName: "blogs" */ '../views/Blogs.vue')
        },
        {
            path: '*',
            redirect: '/',
        }
    ]
});

export default router