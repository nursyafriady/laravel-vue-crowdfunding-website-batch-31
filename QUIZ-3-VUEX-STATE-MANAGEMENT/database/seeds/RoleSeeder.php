<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id' => 'f5dad600',
            'name' => 'user',
        ]);

        Role::create([
            'id' => 1,
            'name' => 'admin',
        ]);
    }
}
