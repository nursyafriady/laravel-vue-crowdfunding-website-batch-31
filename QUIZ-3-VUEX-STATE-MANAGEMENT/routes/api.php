<?php

use Illuminate\Http\Request;

// Route::get('/tes', 'TestController');
Route::namespace('Auth\API')->group(function () {
    Route::post('/register', 'RegisterController');
    Route::post('/verification', 'VerificationController');
    Route::post('/regenerate-otp', 'RegenerateOtpController');
    Route::post('/update-password', 'PasswordController@update');
    Route::post('/login', 'LoginController');

    Route::namespace('User')->group(function () {
        Route::get('/profile/get-profile', 'UserController@getProfile');
        Route::post('/profile/update-profile', 'UserController@updateProfile');
    });
});

Route::middleware('api')->namespace('API')->group(function () {
    Route::prefix('campaign')->group(function() {
        Route::get('random/{count}', 'CampaignController@random');
        Route::post('store', 'CampaignController@store');
        Route::get('/', 'CampaignController@index');
        Route::get('/{id}', 'CampaignController@detail');
    });
    Route::prefix('blog')->group(function() {
        Route::get('random/{count}', 'BlogController@random');
        Route::post('store', 'BlogController@store');
    });
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
