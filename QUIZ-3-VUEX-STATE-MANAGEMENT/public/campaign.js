(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["campaign"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Detail.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Detail.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_Loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Loader.vue */ "./resources/js/components/Loader.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      campaign: {},
      // donasi: 0,
      message: '',
      loading: false,
      csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    };
  },
  components: {
    Loading: _components_Loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  created: function created() {
    this.getDetail();
  },
  methods: {
    getDetail: function getDetail() {
      var _this = this;

      var id = this.$route.params.id;
      var url = '/api/campaign/' + id;
      this.loading = true;
      axios.get(url).then(function (response) {
        if (response.data) {
          _this.message = response.data.response_message;
          var data = response.data.data;
          _this.campaign = data.campaign;
          _this.loading = false;
        }

        _this.loading = false;
      })["catch"](function (e) {
        console.log(e);
        _this.loading = false;
      });
    },
    donate: function donate() {
      // alert('donasi')
      // this.donasi++ 
      this.$store.commit('increment');
    }
  },
  computed: {
    counter: function counter() {
      return this.$store.getters.getCounter;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Detail.vue?vue&type=template&id=78b841b0&":
/*!****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Detail.vue?vue&type=template&id=78b841b0& ***!
  \****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _vm.loading == true
      ? _c(
          "div",
          { staticClass: "d-flex justify-content-center align-items-center" },
          [_c("Loading")],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _vm.loading == false
      ? _c("div", [
          _vm.campaign.id
            ? _c("div", { staticClass: "row justify-content-center" }, [
                _c("div", { staticClass: "col-md-8" }, [
                  _c(
                    "div",
                    {
                      staticClass: "border rounded shadow border-success card",
                    },
                    [
                      _c("img", {
                        staticClass: "card-img-top",
                        attrs: {
                          src: _vm.campaign.image,
                          alt: "Card image cap",
                        },
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "card-body" }, [
                        _c("h5", { staticClass: "card-title" }, [
                          _vm._v(_vm._s(_vm.campaign.title)),
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "w-full mb-2" },
                          [
                            _c("campaign-item", {
                              attrs: { campaign: _vm.campaign },
                            }),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-md-6 d-flex justify-content-between",
                            },
                            [
                              _vm.campaign.required !== null
                                ? _c(
                                    "button",
                                    { staticClass: "btn btn-primary" },
                                    [
                                      _vm._v(
                                        "\n                                    Dibutuhkan: Rp. " +
                                          _vm._s(
                                            _vm.campaign.required.toLocaleString(
                                              "id-ID"
                                            )
                                          ) +
                                          "\n                                "
                                      ),
                                    ]
                                  )
                                : _c(
                                    "button",
                                    { staticClass: "btn btn-primary" },
                                    [
                                      _c("b-icon", {
                                        attrs: {
                                          icon: "credit-card",
                                          variant: "white",
                                        },
                                      }),
                                      _vm._v(
                                        "\n                                    dibutuhkan: Rp. 0\n                                "
                                      ),
                                    ],
                                    1
                                  ),
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass:
                                "col-md-6 d-flex justify-content-end",
                            },
                            [
                              _vm.campaign.collected !== null
                                ? _c(
                                    "button",
                                    { staticClass: "btn btn-success" },
                                    [
                                      _vm._v(
                                        "\n                                    Terkumpul: Rp. " +
                                          _vm._s(
                                            _vm.campaign.collected.toLocaleString(
                                              "id-ID"
                                            )
                                          ) +
                                          "\n                                "
                                      ),
                                    ]
                                  )
                                : _c(
                                    "button",
                                    { staticClass: "btn btn-success" },
                                    [
                                      _c("b-icon", {
                                        attrs: {
                                          icon: "credit-card",
                                          variant: "white",
                                        },
                                      }),
                                      _vm._v(
                                        "\n                                    terkumpul: Rp. 0\n                                "
                                      ),
                                    ],
                                    1
                                  ),
                            ]
                          ),
                        ]),
                        _vm._v(" "),
                        _c("hr"),
                        _vm._v(" "),
                        _c("p", [
                          _vm._v("Alamat: " + _vm._s(_vm.campaign.address)),
                        ]),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("p", { staticClass: "mt-0 card-text" }, [
                          _vm._v(_vm._s(_vm.campaign.description)),
                        ]),
                      ]),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "m-2 rounded btn btn-warning",
                          attrs: {
                            disabled:
                              _vm.campaign.collected >= _vm.campaign.required &&
                              _vm.campaign.collected > 0,
                          },
                          on: { click: _vm.donate },
                        },
                        [
                          _vm._v(
                            "\n                        DONATE\n                    "
                          ),
                        ]
                      ),
                    ]
                  ),
                ]),
              ])
            : _c("div", { staticClass: "row justify-content-center" }, [
                _c("div", { staticClass: "col-md-8" }, [
                  _c(
                    "div",
                    {
                      staticClass: "border rounded shadow border-success card",
                    },
                    [
                      _c("h3", { staticClass: "text-center card-header" }, [
                        _vm._v("Not Found"),
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "card-body" }, [
                        _c("p", [
                          _vm._v("Oppss,, .... " + _vm._s(_vm.message)),
                        ]),
                      ]),
                    ]
                  ),
                ]),
              ]),
        ])
      : _vm._e(),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Detail.vue":
/*!***************************************!*\
  !*** ./resources/js/views/Detail.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Detail_vue_vue_type_template_id_78b841b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Detail.vue?vue&type=template&id=78b841b0& */ "./resources/js/views/Detail.vue?vue&type=template&id=78b841b0&");
/* harmony import */ var _Detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Detail.vue?vue&type=script&lang=js& */ "./resources/js/views/Detail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Detail_vue_vue_type_template_id_78b841b0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Detail_vue_vue_type_template_id_78b841b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Detail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Detail.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./resources/js/views/Detail.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Detail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Detail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Detail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Detail.vue?vue&type=template&id=78b841b0&":
/*!**********************************************************************!*\
  !*** ./resources/js/views/Detail.vue?vue&type=template&id=78b841b0& ***!
  \**********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Detail_vue_vue_type_template_id_78b841b0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Detail.vue?vue&type=template&id=78b841b0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Detail.vue?vue&type=template&id=78b841b0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Detail_vue_vue_type_template_id_78b841b0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Detail_vue_vue_type_template_id_78b841b0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);