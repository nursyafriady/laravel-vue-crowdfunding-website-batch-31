(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["campaigns"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaigns.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Campaigns.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_Loader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/Loader.vue */ "./resources/js/components/Loader.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      campaigns: [],
      perPage: 2,
      loading: false,
      currentPage: 1,
      pageSize: 0,
      totalPage: 0
    };
  },
  components: {
    Loading: _components_Loader_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  created: function created() {
    // this.getCampaigns()
    this.go();
  },
  methods: {
    // getCampaigns() {
    //     this.loading = true
    //     axios.get('api/campaign', {
    //         params: { perPage: this.perPage}
    //     })
    //         .then((response) => {
    //             this.campaigns = response.data
    //             this.loading = false
    //         })
    //         .catch((e) => {
    //             let {response} = e
    //             this.loading = false
    //             console.log(response)
    //         })
    // },
    go: function go() {
      var _this = this;

      this.loading = true;
      axios.get("api/campaign?page=".concat(this.currentPage)).then(function (response) {
        var data = response.data.data;
        _this.campaigns = data.campaigns.data;
        _this.currentPage = data.campaigns.current_page;
        _this.pageSize = data.campaigns.per_page;
        _this.totalPage = data.campaigns.total;
        _this.loading = false;
      })["catch"](function (e) {
        var response = e.response;
        _this.loading = false;
        console.log(response);
      });
    } // loadMore() {
    //     this.perPage += 2
    //     this.getCampaigns()
    // },
    // lessMore() {
    //     this.perPage -= 2
    //     this.getCampaigns()
    // }

  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaigns.vue?vue&type=template&id=5a8949d4&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Campaigns.vue?vue&type=template&id=5a8949d4& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _vm.loading == true
      ? _c(
          "div",
          { staticClass: "d-flex justify-content-center align-items-center" },
          [_c("Loading")],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _vm.loading == false
      ? _c("div", [
          _c(
            "div",
            { staticClass: "row" },
            _vm._l(_vm.campaigns, function (campaign) {
              return _c(
                "div",
                {
                  key: campaign.id,
                  staticClass: "col-md-6",
                  attrs: {
                    "per-page": _vm.pageSize,
                    "current-page": _vm.currentPage,
                  },
                },
                [
                  _c(
                    "div",
                    {
                      staticClass:
                        "mb-3 border rounded shadow border-success card",
                    },
                    [
                      _c("img", {
                        staticClass: "card-img-top",
                        attrs: { src: campaign.image, alt: "Card image cap" },
                      }),
                      _vm._v(" "),
                      _c(
                        "div",
                        {
                          staticClass: "card-body",
                          staticStyle: { "min-height": "14rem !important" },
                        },
                        [
                          _c("h5", { staticClass: "card-title" }, [
                            _vm._v(_vm._s(campaign.title)),
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "w-full mb-2" },
                            [
                              _c("campaign-item", {
                                attrs: { campaign: campaign },
                              }),
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("div", { staticClass: "row" }, [
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-md-6 d-flex justify-content-between",
                              },
                              [
                                campaign.required !== null
                                  ? _c(
                                      "button",
                                      { staticClass: "btn btn-primary" },
                                      [
                                        _vm._v(
                                          "\n                                    Dibutuhkan: Rp. " +
                                            _vm._s(
                                              campaign.required.toLocaleString(
                                                "id-ID"
                                              )
                                            ) +
                                            "\n                                "
                                        ),
                                      ]
                                    )
                                  : _c(
                                      "button",
                                      { staticClass: "btn btn-primary" },
                                      [
                                        _c("b-icon", {
                                          attrs: {
                                            icon: "credit-card",
                                            variant: "white",
                                          },
                                        }),
                                        _vm._v(
                                          "\n                                    dibutuhkan: Rp. 0\n                                "
                                        ),
                                      ],
                                      1
                                    ),
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                staticClass:
                                  "col-md-6 d-flex justify-content-end",
                              },
                              [
                                campaign.collected !== null
                                  ? _c(
                                      "button",
                                      { staticClass: "btn btn-secondary" },
                                      [
                                        _vm._v(
                                          "\n                                    Terkumpul: Rp. " +
                                            _vm._s(
                                              campaign.collected.toLocaleString(
                                                "id-ID"
                                              )
                                            ) +
                                            "\n                                "
                                        ),
                                      ]
                                    )
                                  : _c(
                                      "button",
                                      { staticClass: "btn btn-secondary" },
                                      [
                                        _c("b-icon", {
                                          attrs: {
                                            icon: "credit-card",
                                            variant: "white",
                                          },
                                        }),
                                        _vm._v(
                                          "\n                                    terkumpul: Rp. 0\n                                "
                                        ),
                                      ],
                                      1
                                    ),
                              ]
                            ),
                          ]),
                          _vm._v(" "),
                          _c("hr"),
                          _vm._v(" "),
                          _c("p", { staticClass: "card-text" }, [
                            _vm._v(
                              _vm._s(campaign.description.substring(0, 200))
                            ),
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "mt-2 d-flex justify-content-end" },
                            [
                              _c(
                                "router-link",
                                {
                                  staticClass: "btn btn-success",
                                  attrs: { to: "/campaign/" + campaign.id },
                                },
                                [_vm._v("Detail")]
                              ),
                            ],
                            1
                          ),
                        ]
                      ),
                    ]
                  ),
                ]
              )
            }),
            0
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt-2 d-flex justify-content-center" },
            [
              _c("b-pagination", {
                attrs: {
                  "total-rows": _vm.totalPage,
                  "per-page": _vm.pageSize,
                  pills: "",
                },
                on: { input: _vm.go },
                model: {
                  value: _vm.currentPage,
                  callback: function ($$v) {
                    _vm.currentPage = $$v
                  },
                  expression: "currentPage",
                },
              }),
            ],
            1
          ),
        ])
      : _vm._e(),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Campaigns.vue":
/*!******************************************!*\
  !*** ./resources/js/views/Campaigns.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Campaigns_vue_vue_type_template_id_5a8949d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Campaigns.vue?vue&type=template&id=5a8949d4& */ "./resources/js/views/Campaigns.vue?vue&type=template&id=5a8949d4&");
/* harmony import */ var _Campaigns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Campaigns.vue?vue&type=script&lang=js& */ "./resources/js/views/Campaigns.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Campaigns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Campaigns_vue_vue_type_template_id_5a8949d4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Campaigns_vue_vue_type_template_id_5a8949d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Campaigns.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Campaigns.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/views/Campaigns.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaigns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Campaigns.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaigns.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaigns_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Campaigns.vue?vue&type=template&id=5a8949d4&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/Campaigns.vue?vue&type=template&id=5a8949d4& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaigns_vue_vue_type_template_id_5a8949d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Campaigns.vue?vue&type=template&id=5a8949d4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Campaigns.vue?vue&type=template&id=5a8949d4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaigns_vue_vue_type_template_id_5a8949d4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Campaigns_vue_vue_type_template_id_5a8949d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);