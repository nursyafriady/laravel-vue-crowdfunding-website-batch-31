<?php

namespace App\Http\Controllers\API;

use App\Blog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function random($count)
    {
        $blogs = Blog::select('*')
                ->inRandomOrder()
                ->limit($count)
                ->get();
        
        $data['blogs'] = $blogs;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blogs berhasil ditampilkan',
            'data' => $data
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg, jpeg, png'
        ]);

        $blog = Blog::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        if (request('image')) {    
            $image = request()->file('image')->store('blogs_photo'); 

            try {
                $blog->update([
                    'image' => $image
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'photo profil gagal upload',
                    'data' => $blog
                ], 200);
            }
        }
        $data['blog'] = $blog;
        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blogs berhasil ditambahkan',
            'data' => $data
        ], 200);
    }
}
