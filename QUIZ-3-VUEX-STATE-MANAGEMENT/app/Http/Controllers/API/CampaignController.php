<?php

namespace App\Http\Controllers\API;

use App\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function index()
    {
        // $campaigns = Campaign::paginate(request('perPage'));
        $campaigns = Campaign::paginate(2);
        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaigns berhasil ditampilkan',
            'data' => $data,
            'hasMorePages' => $campaigns->hasMorePages()
        ], 200);
    }
    
    public function random($count)
    {
        $campaigns = Campaign::select('*')
                ->inRandomOrder()
                ->limit($count)
                ->get();
        
        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaigns berhasil ditampilkan',
            'data' => $data
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg, jpeg, png'
        ]);

        $campaign = Campaign::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        if (request('image')) {    
            $image = request()->file('image')->store('campaigns_photo'); 

            try {
                $campaign->update([
                    'image' => $image
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'photo profil gagal upload',
                    'data' => $campaign
                ], 200);
            }
        }
        $data['campaign'] = $campaign;
        return response()->json([
            'response_code' => '00',
            'response_message' => 'data campaigns berhasil ditambahkan',
            'data' => $data
        ], 200);
    }

    public function detail($id)
    {
        try {
            $campaign = Campaign::findOrFail($id);
            $data['campaign'] = $campaign;    
            return response()->json([
                'response_code' => '00',
                'response_message' => 'data detail campaign berhasil ditampilkan',
                'data' => $data,
            ], 200);            
        } catch (\Exception $e) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'data detail campaign tidak ada',
            ], 200);
        }
    }
}
