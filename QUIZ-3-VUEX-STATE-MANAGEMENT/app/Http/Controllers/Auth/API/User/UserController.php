<?php

namespace App\Http\Controllers\Auth\API\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
// use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth:api', 'mail']);
    }
    
    public function getProfile (Request $request)
    {
        $user = $request->user();

        return response()->json([
            'response_code' => "00",
            'response_message' => 'Profile Berhasil Ditampilkan',
            'data' => [
                'profile' => new UserResource($user),
            ]
        ], 200);
    }

    public function updateProfile()
    {
        request()->validate([
            'name' => ['required'],
            'photo' => ['image', 'max:2048']
        ]);

        $user = Auth::user();
        // return $user->photo;

        if (request('photo')) {
            if ($user->photo) {
                $path = parse_url($user->photo);
                File::delete(public_path($path['path']));
                // Storage::delete($user->photo);
            }    
            $photo = request()->file('photo')->store('profile_photo_user');          
        } else {
            $photo = $user->photo;
        }        

        $user->update([
            'name' => request('name'),
            'photo' => $photo
        ]);
        
        return response()->json([
            'response_code' => "00",
            'response_message' => 'Berhasil Update Profile',
            'data' => [
                'user' => new UserResource($user),
            ]
        ], 200);
    }
}
