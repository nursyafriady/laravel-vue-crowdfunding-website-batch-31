<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UserRegisteredSendOtpCode implements ShouldQueue
{   
    public function __construct()
    {
        //
    }

    public function handle(UserRegisteredEvent $event)
    {
        return $event->otpCode;
    }
}
