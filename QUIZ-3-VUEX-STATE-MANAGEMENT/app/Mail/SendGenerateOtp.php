<?php

namespace App\Mail;

use App\User;
use App\Otp_Code;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendGenerateOtp extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->from('funding@gmail.com')
                    ->view('emails.regenarated-otp')
                    ->with([
                        'name' => $this->user->name,
                        'otpCode' => $this->user->otp_code->otp,
                    ]);              
    }
}
