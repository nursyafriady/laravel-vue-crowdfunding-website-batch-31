<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegisteredEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->from('funding@gmail.com')
                    ->view('emails.user-registered-email')
                    ->with([
                        'name' => $this->user->name,
                        'otpCode' => $this->user->otp_code->otp,
                        ]);
    }
}
