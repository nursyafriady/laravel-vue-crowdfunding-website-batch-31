<?php

namespace App;

use Illuminate\Support\Str;
use App\Events\ReGenerateOtpEvent;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];
    protected $fillable = ['role_id', 'name', 'username', 'photo', 'email', 'password'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();
        // getKeyName() == mengambil this perimary key
        static::creating(function($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    // public function role()
    // {
    //     return $this->hasOne(Role::class);
    // }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function otp_code()
    {
        return $this->hasOne(Otp_Code::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getPhotoAttribute()
    {
        if(isset($this->attributes['photo'])) {
            return url('') . '/storage/'.$this->attributes['photo'];
        }

        return null;
    }

    protected $dispatchesEvents = [ 
        'newOtp' => ReGenerateOtpEvent::class 
    ];
}
